#include <iostream>
#include <exception>


using namespace std;

class DequeEmptyException;
class SizeOfDequeException;

template <typename T>
class Wezel
{
private:
	T wartosc;
	Wezel * nastepny;
	Wezel * poprzedni;

public:

    /******************metody dostepu do zmiennych*******************/


    T getWartosc() {return wartosc;} //metoda dostapu do wartosci
    Wezel* getNastepny() {return nastepny;} //metoda dostepu do nastepnego
    Wezel* getPoprzedni() {return poprzedni;} //metoda dostepu do poprzedniego
    void setWartosc(T nowyW) {wartosc = nowyW;} //metoda modyfikujaca wartosc
    void setNastepny(Wezel<T> *nowyN) {nastepny = nowyN;} //metoda modyfikuja nastepny
    void setPoprzedni(Wezel<T> *nowyP) {poprzedni = nowyP;} //metoda modyfikuja poprzedni

    Wezel()
	{
		wartosc = 0;
		nastepny = NULL;
		poprzedni = NULL;
	}
};

template <typename T>
class Deque: public Wezel<T>
{
private:
    Wezel <T> *glowa;
    Wezel <T> *ogon;
    int rozmiar;

public:

    /******************metody dostepu do zmiennych*******************/

    Wezel<T>* getGlowa() {return glowa;} //metoda dostepu do glowy
    Wezel<T>* getOgon() {return ogon;} //metoda dostepu do ogona
    int getRozmiar() {return rozmiar;} //metoda dostepu do rozmiaru
    void setGlowa(Wezel<T> *nowyG) {glowa = nowyG;} //metoda modyfikujaca glowa
    void setOgon(Wezel<T> *nowyO) {ogon = nowyO;} //metoda modyfikujaca ogon
    void setRozmiar(int r) {rozmiar = r;} //metoda modyfikujaca liczby elementow

    int size(); //Zwraca ilo�� obiekt�w przechowywanych w deque
    bool isEmpty(); //Zwraca true je�li deque jest pusty
    T front(); // Zwraca pierwszy obiekt w deque. (Wyrzuca DequeEmptyException je�li deque jest pusta)
    T back(); // Zwraca ostatni obiekt w deque. (Wyrzuca DequeEmptyException je�li deque jest pusta)
    void addFront(T); //Dodaje obiekt do poczatku deque�a. IN: element, ktory chcemy wstawic
    void removeFront(); // Usuwa pierwszy obiekt z deque. (Wyrzuca DequeEmptyException je�li deque jest pusta)
    void addBack(T); //Dodaje obiekt na ko�cu deque�a. IN: element, ktory chcemy wstawic
    void removeBack(); //Usuwa ostatni obiekt z deque. (Wyrzuca DequeEmptyException je�li deque jest pusta)
    void insert(int,T); //Dodaje element w miejsce o podanym indeksie i wartosci.
    void erase(int); //Usuwa element o podanym indeksie. IN: indeks elementu ktory chemu usunac
    void clear(); //Usuwa wszystkie elementy
    T operator [] (int); //zwraca element listy o podanym indeksie (Wyrzuca SizeOfListException je�li deque jest pusta)
    void show(); //wyswietla kolejke


    Deque() //konstruktor
    {
        glowa = NULL;
        ogon = NULL;
        rozmiar = 0;
    }
    ~Deque() //destruktor
    {
        clear();
    }
};

template <typename T>
int Deque<T>::size()
{
    return rozmiar;
}

template <typename T>
bool Deque<T>::isEmpty()
{
    if(rozmiar==0) return true;
    else return false;
}

template <typename T>
T Deque<T>::front()
{
    if(rozmiar==0) throw DequeEmptyException();
    else return glowa->getWartosc();
}

template <typename T>
T Deque<T>::back()
{
    if(rozmiar==0) throw DequeEmptyException();
    else return ogon->getWartosc();
}

template <typename T>
void Deque<T>::addFront(T elem)
{
	Wezel <T> *nowy = new Wezel <T>;
    nowy->setWartosc(elem); //ustawia wartosc kontenera

    if(rozmiar==0)
    {
        glowa = nowy;
        ogon = nowy;
    }
    else
    {
        nowy->setNastepny(glowa);
        glowa->setPoprzedni(nowy);
        glowa = nowy;
    }
    rozmiar++;
}

template <typename T>
void Deque<T>::removeFront()
{
    Wezel <T> *wsk = glowa;
    if(rozmiar==0) throw DequeEmptyException();

    glowa = glowa->getNastepny();
    delete wsk;
    --rozmiar;
}

template <typename T>
void Deque<T>::addBack(T elem)
{
	Wezel <T> *nowy = new Wezel <T>;
    nowy->setWartosc(elem); //ustawia wartosc kontenera

    if(rozmiar==0)
    {
        glowa=nowy;
        ogon=nowy;
    }
    else
    {
        ogon->setNastepny(nowy);
        nowy->setPoprzedni(ogon);
        ogon = nowy;
    }
    rozmiar++;
}

template <typename T>
void Deque<T>::removeBack()
{
    Wezel <T> *wsk = ogon;
    if(rozmiar==0) throw DequeEmptyException();

    ogon = ogon->getPoprzedni();
    delete wsk;
    rozmiar--;
}

template <typename T>
void Deque<T>::erase(int index)
{
    if(index >= rozmiar || index < 0) throw SizeOfDequeException();
    else if (index == 0) removeFront();
    else if (index == rozmiar-1) removeBack();
    else
    {
        Wezel<T> *wsk1 = glowa;
        Wezel<T> *wsk2 = glowa->getNastepny();
        Wezel<T> *wsk3 = glowa->getNastepny()->getNastepny();
        for(int i=1; i<index; i++)
        {
            wsk1 = wsk1->getNastepny();
            wsk2 = wsk2->getNastepny();
            wsk3 = wsk3->getNastepny();
        }

        wsk1->setNastepny(wsk3);
        wsk3->setPoprzedni(wsk1);
        delete wsk2;
        rozmiar--;
    }
}

template <typename T>
void Deque<T>::insert(int index, T wart)
{
    if(index >= rozmiar || index < 0) throw SizeOfDequeException();
    else if (index == 0) addFront(wart);
    else if (index == rozmiar-1) addBack(wart);
    else
    {
        Wezel<T> *wsk1 = glowa;
        Wezel<T> *wsk2 = glowa->getNastepny();
        Wezel<T> *nowy = new Wezel<T>;
        for(int i=1; i<index; i++)
        {
            wsk1 = wsk1->getNastepny();
            wsk2 = wsk2->getNastepny();
        }

        nowy->setWartosc(wart);
        nowy->setPoprzedni(wsk1);
        nowy->setNastepny(wsk2);
        wsk1->setNastepny(nowy);
        wsk2->setPoprzedni(nowy);

        rozmiar++;
    }
}

template <typename T>
void Deque<T>::clear()
{
    while(!isEmpty()) removeFront();
}

template <typename T>
T Deque<T>::operator[](int index)
{
    Wezel<T>* wsk = glowa;
    if(index < 0 || index >= rozmiar) throw SizeOfDequeException();
    else
    {
        for(int i=0; i<index; i++)
            wsk = wsk->getNastepny();

        return wsk->getWartosc();
    }
}

template <typename T>
void Deque<T>::show()
{
    Wezel <T> *wsk = glowa;
    cout << "KOLEJKA: " << endl;
    cout << "------------------------" << endl;
    for(int i=0; i<size(); i++)
    {
       cout << wsk->getWartosc() << " ";
       wsk = wsk->getNastepny();
    }
    cout << endl << "------------------------" << endl << endl;
}

class DequeEmptyException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Lista jest pusta.";
	}
};

class SizeOfDequeException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Przekroczyles rozmiar tablicy.";
	}
};
