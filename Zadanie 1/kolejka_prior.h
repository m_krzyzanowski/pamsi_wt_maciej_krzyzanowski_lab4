#include <iostream>
#include <exception>


using namespace std;

class SekwencjaEmptyException;
class SizeOfSekwencjaException;

template <typename T>
class Wezel
{
private:
	T wartosc;
    int klucz;
	Wezel *nastepny;
	Wezel *poprzedni;


public:

    /******************metody dostepu do zmiennych*******************/


    T getWartosc() {return wartosc;} //metoda dostapu do wartosci
    int getKlucz() {return klucz;} //metoda dostepu do klucza
    Wezel* getNastepny() {return nastepny;} //metoda dostepu do nastepnego
    Wezel* getPoprzedni() {return poprzedni;} //metoda dostepu do poprzedniego
    void setWartosc(T nowyW) {wartosc = nowyW;} //metoda modyfikujaca wartosc
    void setKlucz(int k) {klucz = k;} //metoda modyfikujaca klucz
    void setNastepny(Wezel<T> *nowyN) {nastepny = nowyN;} //metoda modyfikuja nastepny
    void setPoprzedni(Wezel<T> *nowyP) {poprzedni = nowyP;} //metoda modyfikuja poprzedni

    Wezel()
	{
		wartosc = 0;
		klucz = 0;
		nastepny = NULL;
		poprzedni = NULL;
	}
};

template <typename T>
class Sekwencja: public Wezel<T>
{
private:
    Wezel <T> *glowa;
    Wezel <T> *ogon;
    int rozmiar;

public:

    /******************metody dostepu do zmiennych*******************/

    Wezel<T>* getGlowa() {return glowa;} //metoda dostepu do glowy
    Wezel<T>* getOgon() {return ogon;} //metoda dostepu do ogona
    int getRozmiar() {return rozmiar;} //metoda dostepu do rozmiaru
    void setGlowa(Wezel<T> *nowyG) {glowa = nowyG;} //metoda modyfikujaca glowa
    void setOgon(Wezel<T> *nowyO) {ogon = nowyO;} //metoda modyfikujaca ogon
    void setRozmiar(int r) {rozmiar = r;} //metoda modyfikujaca liczby elementow

    int size(); //Zwraca ilo�� obiekt�w przechowywanych w Sekwencja
    bool isEmpty(); //Zwraca true je�li Sekwencja jest pusty
    T front(); // Zwraca pierwszy obiekt w Sekwencja. (Wyrzuca SekwencjaEmptyException je�li Sekwencja jest pusta)
    T back(); // Zwraca ostatni obiekt w Sekwencja. (Wyrzuca SekwencjaEmptyException je�li Sekwencja jest pusta)
    void addFront(T); //Dodaje obiekt do poczatku Sekwencja�a. IN: element, ktory chcemy wstawic
    void removeFront(); // Usuwa pierwszy obiekt z Sekwencja. (Wyrzuca SekwencjaEmptyException je�li Sekwencja jest pusta)
    void addBack(T); //Dodaje obiekt na ko�cu Sekwencja�a. IN: element, ktory chcemy wstawic
    void removeBack(); //Usuwa ostatni obiekt z Sekwencja. (Wyrzuca SekwencjaEmptyException je�li Sekwencja jest pusta)
    T atIndex(int i) const; //zwraca element z pozycji i
    int indexOf(T p); //zwraca indeks elementu
    void show(); //wyswietla kolejke



    Sekwencja() //konstruktor
    {
        glowa = NULL;
        ogon = NULL;
        rozmiar = 0;
    }
    ~Sekwencja() //destruktor
    {
        Wezel <T> *wsk = glowa;
        while(wsk != NULL)
        {
            delete wsk;
            wsk = wsk->getNastepny();
        }
    }
};

template <typename T>
int Sekwencja<T>::size()
{
    return rozmiar;
}

template <typename T>
bool Sekwencja<T>::isEmpty()
{
    if(rozmiar==0) return true;
    else return false;
}

template <typename T>
T Sekwencja<T>::front()
{
    if(rozmiar==0) throw SekwencjaEmptyException();
    else return glowa->getWartosc();
}

template <typename T>
T Sekwencja<T>::back()
{
    if(rozmiar==0) throw SekwencjaEmptyException();
    else return ogon->getWartosc();
}

template <typename T>
void Sekwencja<T>::addFront(T elem)
{
	Wezel <T> *nowy = new Wezel <T>;
    nowy->setWartosc(elem); //ustawia wartosc kontenera

    if(rozmiar==0)
    {
        glowa = nowy;
        ogon = nowy;
    }
    else
    {
        nowy->setNastepny(glowa);
        glowa->setPoprzedni(nowy);
        glowa = nowy;
    }
    rozmiar++;
}

template <typename T>
void Sekwencja<T>::removeFront()
{
    Wezel <T> *wsk = glowa;
    if(rozmiar==0) throw SekwencjaEmptyException();
    else
    {
        rozmiar--;
        if(rozmiar != 0) glowa = glowa->getNastepny();
        delete wsk;
    }
}

template <typename T>
void Sekwencja<T>::addBack(T elem)
{
	Wezel <T> *nowy = new Wezel <T>;
    nowy->setWartosc(elem); //ustawia wartosc kontenera

    if(rozmiar==0)
    {
        glowa=nowy;
        ogon=nowy;
    }
    else
    {
        ogon->setNastepny(nowy);
        nowy->setPoprzedni(ogon);
        ogon = nowy;
    }
    rozmiar++;
}

template <typename T>
void Sekwencja<T>::removeBack()
{
    Wezel <T> *wsk = ogon;
    if(rozmiar==0) throw SekwencjaEmptyException();
    else
    {
        rozmiar--;
        if(rozmiar != 0) ogon = ogon->getPoprzedni();
        delete wsk;
    }
}

template <typename T>
void Sekwencja<T>::show()
{
    Wezel <T> *wsk = glowa;
    cout << "Sekewncja: " << endl;
    cout << "------------------------" << endl;
    for(int i=0; i<size(); i++)
    {
       cout << wsk->getWartosc() << " ";
       wsk = wsk->getNastepny();
    }
    cout << endl << "------------------------" << endl << endl;
}

template <typename T>
T Sekwencja<T>::atIndex(int i) const
{
    Wezel<T>* wsk = glowa;
    if(isEmpty()) throw SizeOfSekwencjaException();
    else
    {
        for(int j=1; j<i; j++)
            wsk = wsk->getNastepny();
        return wsk->getWartosc();
    }
}

template <typename T>
int Sekwencja<T>::indexOf(T p)
{
    Wezel<T>* wsk = glowa;
    int index = 1;

    while(wsk->getWartosc()!=p && (index < size()))
    {
        ++index;
        wsk = wsk->next;
    }
    if(index==size()) throw SizeOfSekwencjaException();
    else return index;
}

class SekwencjaEmptyException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Kolejka jest pusta.";
	}
};

class SizeOfSekwencjaException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Przekroczyles rozmiar kolejki.";
	}
};


//**************************************************************************************

class SizeOfKolejkaException;

template <typename T>
class KolejkaPrior:public Sekwencja<T>, public Wezel<T>
{

public:
    KolejkaPrior() : Sekwencja<T>::Sekwencja() {}
    void insert(int,T); //dodaje element o (priorytecie,wartosc)
    void removeMin(); //usuwa element o najmniejszym prirytece

    T min() {return Sekwencja<T>::front();} //zwraca ale nie usuwa element o najmniejszym kluczu
    int sizeK() {return Sekwencja<T>::size();} //zwraca ilosc przechowywanych elementow
    bool isEmptyK() {return Sekwencja<T>::isEmpty();}; // sprawdza czy kolejka jest pusta
};

template <typename T>
void KolejkaPrior<T>::insert(int klucz, T wart)
{
     Wezel<T> *wsk = new Wezel<T>;
     int rozm = Sekwencja<T>::getRozmiar(); //zmienna przechowuje rozmiar kolejki
     wsk->setWartosc(wart);
     wsk->setKlucz(klucz);

     if(isEmptyK()) //jezeli kolejka jest pusta
     {
         Sekwencja<T>::setGlowa(wsk);
         Sekwencja<T>::setOgon(wsk);
         Sekwencja<T>::setRozmiar(++rozm);
     }

     else if (Sekwencja<T>::getGlowa()->getKlucz() < klucz) //jezeli element ma najwyzszy klucz
     {
         wsk->setNastepny(Sekwencja<T>::getGlowa());
         Sekwencja<T>::setGlowa(wsk);
         Sekwencja<T>::setRozmiar(++rozm);
     }
     else //jeseli element ma mniejszy priorytet od glowy
     {
        Wezel<T> *pomoc = Sekwencja<T>::getGlowa();

        while((pomoc->getNastepny()) && (pomoc->getNastepny()->getKlucz() >= klucz))
             pomoc = pomoc->getNastepny();

        wsk->setNastepny(pomoc->getNastepny());
        wsk->setPoprzedni(pomoc);
        pomoc->setNastepny(wsk);
        if(wsk->getNastepny()) //jesli to nie jest element na koncu
        {
            pomoc = pomoc->getNastepny()->getNastepny();
            pomoc->setPoprzedni(wsk);
        }
        else Sekwencja<T>::setOgon(wsk);

        Sekwencja<T>::setRozmiar(++rozm);
     }
}

template <typename T>
void KolejkaPrior<T>::removeMin()
{
    if(isEmptyK()) throw SekwencjaEmptyException();
    else Sekwencja<T>::removeFront();
}
